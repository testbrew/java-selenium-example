## Google maps selenium java example 
![Pipeline] [![License]][license_link]

### Main Goal
Example for using Selenium Web Driver in Java, 
analyze creative skills in identifying test cases.

### Description
Testing the search functionality of google maps site. Identify possible test cases for
this feature and create automated tests using the Selenium Web Driver with Java.

### Technology stack
I've used OpenJDK [Java 11] and [Selenium], for testing [JUnit 5] and [Gradle] for build tool.   
Also added [Jacoco] integration so you can check the code coverage under the `build/jacoco/` artifacts.   
For CI I've used [GitLab].   

### How to run locally
Clone project
```bash
$ cd [project_folder]
$ ./gradlew chromeTest -Dq.local=true -Dqa.baseUrl="https://www.google.com/maps?hl=en"
```
> You can use the `test.properties` file to pass project parameters
Don't forget to run `clean` if you would like to clear data before rerunning test locally.
```bash
./gradlew clean 
```
> Currently you can run `chromeTest` or `firefoxTest`. There is also option to run `ieTest` which I haven't tests due to lack of windows machine.

##### Test Output
- Test output: `build/reports`
- Test coverage: `build/jacoco/`
- JUnit xml report: `build/test-results`

##### Coverage HTML report
To generate Jacoco HTML report run `./gradlew jacocoTestReport`. Reports will be located at `build/jacoco/` 

### How to run as Continues Integration
The tests run on each push, for more insight see [gitlab-ci.yml] file.

### Future improvements
- Add integration with [Applitools], or similar, for visual comparison  
- Add cross device testing [Saucelabs] / [lambdaTest] / [BrowserStack]
   
### Issues
1. 

#### LICENSE
This project is licensed under Apache-2.0 License. You are free to use this project and all of it's files.
   
Feel free to give me a star if you find this project educational and beneficial. [Contact me] so I can create more content.

[Java 11]: https://openjdk.java.net/projects/jdk/11/
[Selenium]: https://www.seleniumhq.org/
[gradle]: https://gradle.org/
[junit 5]: https://junit.org/junit5/
[jacoco]: https://www.eclemma.org/jacoco/
[applitools]: https://www.applitools.com
[logo]: https://www.gstatic.com/images/branding/product/2x/maps_512dp.png
[gitlab]: https://gitlab.com/testbrew/java-selenium-example/
[pipeline]: https://gitlab.com/testbrew/java-selenium-example/badges/master/pipeline.svg
[license]: https://img.shields.io/badge/License-Apache%202.0-blue.svg
[license_link]: https://opensource.org/licenses/Apache-2.0
[gitlab-ci.yml]: .gitlab-ci.yml
[Saucelabs]: https://saucelabs.com/
[lambdaTest]: https://www.lambdatest.com/
[BrowserStack]: https://www.browserstack.com/
[contact me]: CONTRIBUTING.md