package com.testbrew.selenium.handle;

import org.openqa.selenium.remote.RemoteWebDriver;

public interface IBrowser {

    RemoteWebDriver create();
}
