package com.testbrew.selenium.pages.google.maps;

import org.openqa.selenium.By;
import org.openqa.selenium.TimeoutException;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.pagefactory.AjaxElementLocatorFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.time.Duration;

public class Page {

    private static final Logger logger = LoggerFactory.getLogger(Page.class);

    protected RemoteWebDriver driver;
    protected WebDriverWait wait;
    private static final int TIMEOUT = 10;
    private static final int POLLING = 1000;

    public Page(RemoteWebDriver driver) {
        this.driver = driver;
        wait = new WebDriverWait(driver, TIMEOUT, POLLING);
        PageFactory.initElements(new AjaxElementLocatorFactory(driver, TIMEOUT), this);
    }

    /**
     * @throws TimeoutException If the timeout expires.
     */
    public void waitForTopResults() {
        wait.withTimeout(Duration.ofSeconds(TIMEOUT)).until(ExpectedConditions.presenceOfElementLocated(By.cssSelector("[aria-label=\"Top Results\"][role=\"listbox\"]")));
    }

    /**
     * @throws TimeoutException If the timeout expires.
     */
    public void waitForBadQueryList() {
        wait.until(ExpectedConditions.presenceOfElementLocated(By.cssSelector("[class=\"section-bad-query\"]")));
    }

    /**
     * Checks for all results, if not exact match is given then we try to have top results
     * Feel free to add your own exact match implementations here.
     *
     * @throws TimeoutException If the timeout expires.
     */
    public void waitForResult() {
        try {
            wait.withTimeout(Duration.ofSeconds(TIMEOUT)).until(ExpectedConditions.presenceOfElementLocated(By.cssSelector("[class=\"section-hero-header-title-description\"]")));
        } catch (TimeoutException e) {
            logger.warn("TimeoutException: no exact match was found");
            logger.info("Searching for top results");
            waitForTopResults();
        }
    }
}
