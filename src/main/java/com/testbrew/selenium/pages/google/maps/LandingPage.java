package com.testbrew.selenium.pages.google.maps;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.FindBy;

public class LandingPage extends Page {

    @FindBy(id = "searchboxinput")
    private WebElement searchBarInput;

    @FindBy(id = "searchbox-searchbutton")
    private WebElement searchBttn;

    public LandingPage(RemoteWebDriver driver) {
        super(driver);
    }

    public LandingPage searchFor(String searchString) {
        searchBarInput.sendKeys(searchString);
        searchBttn.click();
        return this;
    }
}
