package com.testbrew.selenium.browsers;

import com.testbrew.selenium.handle.IBrowser;

import static org.junit.jupiter.api.Assertions.fail;

public class BrowserFactory {

    public IBrowser type(String browserTypeInput){
        if (browserTypeInput == null)
            return null;
        else if (browserTypeInput.equalsIgnoreCase("firefox"))
            return new FirefoxImpl();
        else if (browserTypeInput.equalsIgnoreCase("chrome"))
            return new ChromeImpl();
        else {
            fail(String.format("Browser type %s doesn't match the browsers configured", browserTypeInput ));
        }
        return null;
    }
}