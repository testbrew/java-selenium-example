package com.testbrew.selenium.browsers;

import com.testbrew.selenium.handle.IBrowser;
import org.openqa.selenium.Capabilities;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxOptions;
import org.openqa.selenium.remote.RemoteWebDriver;

public class FirefoxImpl extends BrowserImpl implements IBrowser {

    @Override
    RemoteWebDriver createLocal() {
        return new FirefoxDriver();
    }

    @Override
    RemoteWebDriver createRemote() {
        Capabilities capabilities = new FirefoxOptions();
        return getRemoteDriver(capabilities);
    }
}
