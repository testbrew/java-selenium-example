package com.testbrew.selenium.browsers;

import com.testbrew.selenium.handle.IBrowser;
import org.openqa.selenium.Capabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.net.MalformedURLException;
import java.net.URL;

import static org.junit.jupiter.api.Assertions.fail;

public abstract class BrowserImpl implements IBrowser {

    private static final Logger logger = LoggerFactory.getLogger(BrowserImpl.class);

    private boolean isLocal() {
        return System.getProperty("qa.local").equalsIgnoreCase("true");
    }

    abstract RemoteWebDriver createRemote();

    abstract RemoteWebDriver createLocal();

    public RemoteWebDriver create() {
        if (isLocal())
            return createLocal();
        else
            return createRemote();
    }

    protected RemoteWebDriver getRemoteDriver(Capabilities capabilities) {
        try {
            return new RemoteWebDriver(
                    new URL(String.format("%s/%s", System.getProperty("qa.serverUrl"), "wd/hub")),
                    capabilities
            );
        } catch (MalformedURLException e) {
            logger.warn("Couldn't connect to remote driver");
            fail();
        }
        return null;
    }
}
