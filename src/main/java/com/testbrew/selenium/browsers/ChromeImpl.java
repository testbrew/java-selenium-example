package com.testbrew.selenium.browsers;

import com.testbrew.selenium.handle.IBrowser;
import org.openqa.selenium.Capabilities;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.remote.RemoteWebDriver;

import java.util.concurrent.TimeUnit;

public class ChromeImpl extends BrowserImpl implements IBrowser {

    @Override
    RemoteWebDriver createLocal() {
        ChromeOptions options = new ChromeOptions();
        options.addArguments("--lang=en");
        options.addArguments("--disable-dev-shm-usage");
        options.addArguments("--remote-debugging-port=45447");
        options.addArguments("--no-sandbox");
        options.addArguments("--disable-gpu");
        RemoteWebDriver driver = new ChromeDriver(options);
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        driver.manage().window().maximize();
        return driver;
    }

    @Override
    RemoteWebDriver createRemote() {
        Capabilities capabilities = new ChromeOptions();
        return getRemoteDriver(capabilities);
    }
}
