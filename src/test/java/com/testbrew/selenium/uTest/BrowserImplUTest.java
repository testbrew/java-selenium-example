package com.testbrew.selenium.uTest;

import com.testbrew.selenium.browsers.ChromeImpl;
import com.testbrew.selenium.browsers.FirefoxImpl;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Test;
import org.opentest4j.AssertionFailedError;

import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertThrows;

class BrowserImplUTest {

    private String defaultUrl;

    private BrowserImplUTest() {
        this.defaultUrl = System.getProperty("qa.serverUrl");
    }

    @AfterEach
    private void setPropertyToDefault() {
        System.setProperty("qa.serverUrl", defaultUrl);
    }

    @Test
    void getMalformedURLException() {
        assertThrows(AssertionFailedError.class, () -> {
            System.setProperty("qa.local", "false");
            System.setProperty("qa.serverUrl", "localhost:4444");
            assertNull(new ChromeImpl().create());
        });

    }

    @Test
    void getNullPointerException() {
        assertThrows(NullPointerException.class, () -> {
            System.setProperty("qa.local", "false");
            System.setProperty("qa.serverUrl", null);
            new FirefoxImpl().create();
        });
    }
}
