package com.testbrew.selenium.uTest;

import com.testbrew.selenium.browsers.BrowserFactory;
import org.junit.jupiter.api.Test;
import org.opentest4j.AssertionFailedError;

import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertThrows;

class BrowserFactoryUTest {

    @Test
    void browserTypeInputNull() {
        assertNull(new BrowserFactory().type(null));
    }

    @Test
    void browserTypeInputNotExist() {
        assertThrows(AssertionFailedError.class, () -> new BrowserFactory().type("testbrew.com"));

    }
}
