package com.testbrew.selenium.search.postalcode;

import com.testbrew.selenium.base.BaseTest;
import com.testbrew.selenium.pages.google.maps.LandingPage;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;
import org.openqa.selenium.TimeoutException;

public class PostalCodePositiveFlows extends BaseTest {
    @DisplayName("Search with postal code")
    @ParameterizedTest(name = "Searching with postal code \"{0}\"")
    @CsvSource({
            "16818",
            "36304",
            "56357"
    })
    void searchWithPostalCode(String postalCode) {
        try {
            driver.get(getBaseUrl());
            LandingPage landingPage = new LandingPage(driver);
            landingPage.searchFor(postalCode).waitForResult();
        } catch (TimeoutException e) {
            takeScreenshot(postalCode.replace(" ", "_"));
            throw e;
        }
    }

}