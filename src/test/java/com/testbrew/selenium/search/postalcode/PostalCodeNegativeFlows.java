package com.testbrew.selenium.search.postalcode;

import com.testbrew.selenium.base.BaseTest;
import com.testbrew.selenium.pages.google.maps.LandingPage;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;
import org.openqa.selenium.TimeoutException;

public class PostalCodeNegativeFlows extends BaseTest {


    @DisplayName("Search for one string in address no result expected")
    @ParameterizedTest(name = "Searching with one string in address \"{0}\"")
    @CsvSource({
            "00001",
            "-10001",
            "1o856"
    })
    void searchWithIllegalArguments(String partialAddress) {
        try {
            driver.get(getBaseUrl());
            LandingPage landingPage = new LandingPage(driver);
            landingPage.searchFor(partialAddress).waitForBadQueryList();
        } catch (TimeoutException e) {
            takeScreenshot(partialAddress.replace(" ", "_"));
            throw e;
        }
    }
}