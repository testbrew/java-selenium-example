package com.testbrew.selenium.search.address;

import com.testbrew.selenium.base.BaseTest;
import com.testbrew.selenium.pages.google.maps.LandingPage;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;
import org.openqa.selenium.TimeoutException;

public class AddressNegativeFlows extends BaseTest {


    @DisplayName("Search for one string in address no result expected")
    @ParameterizedTest(name = "Searching with one string in address \"{0}\"")
    @CsvSource({
            "'@%@#$%WEFW'",
            "' '",
            "window.alert(\"bug!!\");",
            "n1wHutiFP4lQ*bV5z35pG&qwRgYZ96vz2^FvZJbgw4WviVi1E0I!4Yp8m4*LeFj45E1lud0181Pi0eprh$*HVM@yOwrTnuEWXjTwptSDi&Qaq9A0QyVlU2T@ALEfsHm67$ASLDNeo^zMeDCd7f3bx!XO48OaZwAtAtLVkhuubTJ30xJAl5vSw@@orzgPbmf6KQNBVq2O"
    })
    void inputWithIllegalChars(String partialAddress) {
        try {
            driver.get(getBaseUrl());
            LandingPage landingPage = new LandingPage(driver);
            landingPage.searchFor(partialAddress).waitForBadQueryList();
        } catch (TimeoutException e) {
            takeScreenshot(partialAddress.replace(" ", "_"));
            throw e;
        }
    }

}
