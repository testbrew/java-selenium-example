package com.testbrew.selenium.search.address;

import com.testbrew.selenium.base.BaseTest;
import com.testbrew.selenium.pages.google.maps.LandingPage;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;
import org.openqa.selenium.TimeoutException;

public class AddressPositiveFlows extends BaseTest {

    @DisplayName("Search for full addresses")
    @ParameterizedTest(name = "Searching for full address \"{0}\"")
    @CsvSource({
            "'Bergheimer Platz 7, 14197, Berlin'",
            "'Immanuel-Kant-Straße 90, 09337 Hohenstein-Ernstthal'",
            "'Ruhlsdorfer Weg 22, 14979 Großbeeren'"
    })
    void searchForFullAddress(String address) {
        _searchSteps(address);
    }

    @DisplayName("Search for partial addresses")
    @ParameterizedTest(name = "Searching for partial address \"{0}\"")
    @CsvSource({
            "'Bergheimer Platz 7'",
            "'Immanuel-Kant-Straße 90 , Hohenstein-Ernstthal'",
            "'Ruhlsdorfer Weg 2'"
    })
    void searchForPartialAddress(String partialAddress) {
        _searchSteps(partialAddress).waitForResult();
    }

    @DisplayName("Search for special case addresses Umlaut")
    @ParameterizedTest(name = "Searching with address with Umlaut \"{1}\" in {0} name")
    @CsvSource({
            "'Immanuel-Kant-Strasse 90, Hohenstein-Ernstthal'",
            "'Sudkreuz Bf, Berlin'",
            "'Ruhlsdörfer Weg 2 Grossbeeren'",
            "'Mellingbürgredder 10'"
    })
    void fullAddressUmlauts(String partialAddress) {
        _searchSteps(partialAddress).waitForResult();
    }


    @DisplayName("Search for one string in address")
    @ParameterizedTest(name = "Searching with one string in address \"{0}\"")
    @CsvSource({
            "'Sudkreuz'",
            "'Ruhlsdörfer Staße'", // boom
            "'Mellingbürgredder'"
    })
    void addressOneNameOnly(String partialAddress) {
        _searchSteps(partialAddress).waitForResult();
    }

    @DisplayName("Search for number in address")
    @ParameterizedTest(name = "Searching with one number address \"{0}\"")
    @CsvSource({
            "'123'"
    })
    void addressHouseNumberOnly(String partialAddress) {
        _searchSteps(partialAddress).waitForTopResults();
    }

    private LandingPage _searchSteps(String partialAddress) {
        try {
            driver.get(getBaseUrl());
            LandingPage landingPage = new LandingPage(driver);
            landingPage.searchFor(partialAddress);
            return landingPage;
        } catch (TimeoutException e) {
            takeScreenshot(partialAddress.replace(" ", "_"));
            throw e;
        }
    }
}
