package com.testbrew.selenium.base;

import com.testbrew.selenium.browsers.BrowserFactory;
import org.apache.commons.io.FileUtils;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.IOException;

public abstract class BaseTest {

    protected static RemoteWebDriver driver;
    private static Logger logger = LoggerFactory.getLogger(BaseTest.class);
    private static String baseUrl = null;

    protected String getBaseUrl() {
        if (baseUrl == null)
            baseUrl = System.getProperty("qa.baseUrl");
        return baseUrl;
    }

    @BeforeAll
    public static void startBrowser() {
        String browserTypeInput = System.getProperty("qa.browser");
        driver = new BrowserFactory().type(browserTypeInput).create();
    }

    @AfterAll
    public static void tearDown() {
        if (driver != null)
            driver.quit();
    }

    @AfterEach
    public void testCleanup() {
        if (driver != null)
            driver.manage().deleteAllCookies();
    }

    /**
     * Function to take screenshots of the test step, this can be saved or analyzed lated
     * @param fileName: file name to be stored
     * @return this object
     */
    protected BaseTest takeScreenshot(String fileName) {
        try {
            File file = driver.getScreenshotAs(OutputType.FILE);
            String newFilePath = String.join(File.separator, "build", "reports", "snapshots", String.format("%s.png",fileName));
            FileUtils.copyFile(file, new File(newFilePath));
        } catch (IOException e) {
            logger.warn("IEOException caught when taking snapshot");
            logger.warn("message: \n{}", e.getMessage());
        }
        return this;
    }

    protected BaseTest takeScreenshot() {
        return takeScreenshot(String.valueOf(System.currentTimeMillis()));
    }
}
